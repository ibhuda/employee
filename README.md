Project Description
=====================================
Project name: Employee Management

Project developed using PHP Laravel and Vue.js

Project still under development.

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
